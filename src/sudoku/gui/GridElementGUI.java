package sudoku.gui;

import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.BorderFactory;
import javax.swing.JPanel;

import sudoku.CONST;
import sudoku.STATE;
import sudoku.Grid;
import sudoku.GridElement;

public class GridElementGUI extends GridElement  {

	private Grid grid;
	private JPanel gui;
	private HelperNumberWithPopup[] helperNumbersWithPopup;
	private STATE lastState;

	public GridElementGUI(int col, int row, Grid grid) {
		super(col, row);
		this.lastState = STATE.POSSIBLE;
		this.grid = grid;

		this.gui = new JPanel();
		this.gui.setBorder(BorderFactory
				.createLineBorder(CONST.COLOR_GRID_ELEMENT_BORDER));
		this.gui.setLayout(new GridLayout(CONST.NUMBER_OF_SQUARES_IN_LENGTH, CONST.NUMBER_OF_SQUARES_IN_LENGTH));
		this.helperNumbersWithPopup = new HelperNumberWithPopup[CONST.GRID_LENGTH];
		for (int i = 0; i < CONST.GRID_LENGTH; i++) {
			helperNumbersWithPopup[i] = new HelperNumberWithPopup(i, this);
			gui.add(helperNumbersWithPopup[i]);
		}

		this.gui.addMouseListener(new MouseAdapter() {
			public void mouseEntered(MouseEvent e) {
				//System.out.println("public void mouseEntered(MouseEvent e)");
				setBackgroundcolorForConnections(CONST.COLOR_HIGHLIGHT);
				setBackgroundcolorForEqualElements(CONST.COLOR_EQUALS);
				setBackgroundcolorForEqualElementsInConnection(Color.RED);
			}
			public void mouseExited(MouseEvent e) {
				setBackgroundcolorForConnections(CONST.COLOR_BACKGROUND);
				setBackgroundcolorForEqualElements(CONST.COLOR_BACKGROUND);
				//setBackgroundcolorForEqualElementsInConnection(Color.RED);
			}
			public void mouseClicked(MouseEvent e) {
				((HelperNumberWithPopup)gui.findComponentAt(e.getX(),e.getY())).showPopupMenu(e);
			}
		});
	}

	public JPanel getGui() {
		return gui;
	}

	public Grid getGrid() {
		return grid;
	}

	@Override
	public void setState(STATE state) {
		this.lastState = this.getState();
		super.setState(state);
		reloadGUI();
	}

	@Override
	public void setHelperStateForNumber(int number, STATE state) {
		this.lastState = this.getState();
		super.setHelperStateForNumber(number, state);
		reloadGUI();
	}
	
	@Override
	public void setCalculatedFix() {
		super.setCalculatedFix();
		reloadGUI();
	}
	
	@Override
	public void resetAllHelperStates() {
		super.resetAllHelperStates();
		reloadGUI();
	}
	
	@Override
	public void reset() {
		super.reset();
		gui.removeAll();
		gui.setLayout(new GridLayout(CONST.NUMBER_OF_SQUARES_IN_LENGTH, CONST.NUMBER_OF_SQUARES_IN_LENGTH));
		for (int i = 0; i < CONST.GRID_LENGTH; i++)
			gui.add(helperNumbersWithPopup[i]);
		for (Component p : gui.getComponents()) 
			((HelperNumberWithPopup)p).reloadStates();
		lastState = STATE.POSSIBLE;
	}


	private void reloadGUI() {
		if  (lastState.isNotFixed() && getState().isFixed()) {
			gui.removeAll();
			gui.setLayout(new GridLayout(1, 1));
			super.setCalculatedFix();
			//miniGrid[this.getFix()-1].reloadStates();
			gui.add(helperNumbersWithPopup[this.getFixedNumber()]);
		}
		if (lastState.isFixed() && getState().isNotFixed()) {
			gui.removeAll();
			gui.setLayout(new GridLayout(CONST.NUMBER_OF_SQUARES_IN_LENGTH, CONST.NUMBER_OF_SQUARES_IN_LENGTH));
			for (int i = 0; i < CONST.GRID_LENGTH; i++) {
				//miniGrid[i].reloadStates();
				gui.add(helperNumbersWithPopup[i]);
			}
		}
		for (Component p : gui.getComponents()) 
			((HelperNumberWithPopup)p).reloadStates();
		gui.validate();
		gui.repaint();
	}

	private void setBackgroundcolorForConnections(Color color) {
		for (GridElement gE : grid.getRow(getRow())) {
			//System.out.println(getRow());
			((GridElementGUI)gE).getGui().setBackground(color);
		}
		for (GridElement gE : getGrid().getCol(getCol())) {
			((GridElementGUI)gE).getGui().setBackground(color);
		}
		for (GridElement gE : getGrid().getSquareContainingGridElemnetAt(getRow(),getCol())) {
			((GridElementGUI)gE).getGui().setBackground(color);
		}
	}

	private void setBackgroundcolorForEqualElements(Color color) {
		if (getState().isNotFixed()) return;
		for (GridElement gE : getGrid().getEqualFixed(getRow(),getCol())) {
			((GridElementGUI)gE).getGui().setBackground(color);
		}
	}

	private void setBackgroundcolorForEqualElementsInConnection(Color color) {
		if (getState().isNotFixed()) return;
		for (GridElement gE : getGrid().getElementsHavingCollitionWithFixed(getRow(),getCol())) {
			((GridElementGUI)gE).getGui().setBackground(color);
		}
	}
}
