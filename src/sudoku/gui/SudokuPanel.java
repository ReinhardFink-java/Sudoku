package sudoku.gui;

import java.awt.BorderLayout;

import java.awt.GridLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JButton;
import javax.swing.JMenuBar;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import sudoku.CONST;
import sudoku.Message;
import sudoku.solver.Solver;


@SuppressWarnings("serial")
public class SudokuPanel extends JPanel {
	
	private GridGUI gridGUI;
	private MenuBar menuBar;
	private Solver solver;
	private JTextArea loggingTextArea;

	public SudokuPanel() {
		super();
		this.createGUI();
		this.menuBar = new MenuBar(this);
		this.solver = new Solver(gridGUI.getGrid());
		//createMenuBar();
	}
	
	public GridGUI getGridGUI() {
		return gridGUI;
	}

	public JMenuBar getMenuBar() {
		return menuBar;
	}

	private void createGUI() {
		this.setLayout(new BorderLayout());
		gridGUI = new GridGUI();
		this.add(gridGUI,BorderLayout.CENTER);
		JPanel buttonPanel = new JPanel(new GridLayout(6,1));
		buttonPanel.add(createButtonResetHelperStates());
		buttonPanel.add(createButtonShowFixed());
		buttonPanel.add(createButtonCalculateFixedFromElement());
		buttonPanel.add(createButtonCalculateFixedFromConnection());
		buttonPanel.add(createButtonCalculateBlockedFromGrid());
		buttonPanel.add(calculateBlockedFromDoubleDouble());
		this.add(buttonPanel,BorderLayout.EAST);

		loggingTextArea = new JTextArea(10,14);
		JScrollPane aJScrollPane = new JScrollPane(loggingTextArea);
		this.add(aJScrollPane,BorderLayout.SOUTH);
		Message.connectJTextFieldToMessage(loggingTextArea);
	}

	private JButton createButtonResetHelperStates() {
		JButton buttonResetBlocked = new JButton(CONST.LABEL_BUTTON_RESET_BLOCKED);
		buttonResetBlocked.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				solver.resetAllHelperStates();
			}
		});
		return buttonResetBlocked;
	}

	private JButton createButtonShowFixed() {
		JButton buttonShowFixed = new JButton(CONST.LABEL_BUTTON_SHOW_BLOCKED);
		buttonShowFixed.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				solver.blockHelperNumbersFromFixedElement();
				//System.out.println("buttonShowFixe");
			}
		});
		return buttonShowFixed;
	}

	private JButton createButtonCalculateFixedFromElement() {
		JButton buttonShowCalculatedFromElement = new JButton(CONST.LABEL_BUTTON_CALC_FIX_FROM_ELEMENT);
		buttonShowCalculatedFromElement.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				solver.fixUniqueNotBlockedInElement();
				//System.out.println("calc Fix from Element");
			}
		});
		return buttonShowCalculatedFromElement;
	}

	private JButton createButtonCalculateFixedFromConnection() {
		JButton buttonShowCalculatedFromGroup = new JButton(CONST.LABEL_BUTTON_CALC_FIX_FROM_CONNECTION);
		buttonShowCalculatedFromGroup.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				solver.fixElementsFromBlockedInRowColSquare();
				//System.out.println("calc Fix from Group");
			}
		});
		return buttonShowCalculatedFromGroup;
	}

	private JButton createButtonCalculateBlockedFromGrid() {
		JButton buttonBlockFromGrid = new JButton(CONST.LABEL_BUTTON_CALC_FIX_FROM_SQUARE);
		buttonBlockFromGrid.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				solver.setBlockedElementsInLinesFromSqaures();
				//System.out.println("calc Fix from Group");
			}
		});
		return buttonBlockFromGrid;
	}

	private JButton calculateBlockedFromDoubleDouble() {
		JButton buttonBlockFromDoublesInGrid = new JButton("block from Doubles");
		buttonBlockFromDoublesInGrid.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				solver.setBlockedElementsFromDoublesDoublesInConnections();
			}
		});
		return buttonBlockFromDoublesInGrid;
	}
}
