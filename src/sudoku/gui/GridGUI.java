package sudoku.gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;

import javax.swing.BorderFactory;
import javax.swing.JPanel;

import sudoku.CONST;
import sudoku.Grid;


@SuppressWarnings("serial")
public class GridGUI extends JPanel{
	
	private Grid grid;

	public GridGUI() {
		super();
		createGrid();
		createGridGUI();
	}

	public Grid getGrid() {
		return grid;
	}

	public void createGrid() {
		grid = new Grid(CONST.GRID_LENGTH,CONST.GRID_LENGTH);
		for (int row = 0; row < CONST.GRID_LENGTH; row++)
			for (int col = 0; col < CONST.GRID_LENGTH; col++)
				grid.setElementAt(row, col, new GridElementGUI(row,col, grid));
	}
	
	public void reset() {
		grid.reset();
	}

	private void createGridGUI() {
		this.setSize(new Dimension(7000, 600));
		this.setLayout(new GridLayout(CONST.NUMBER_OF_SQUARES_IN_LENGTH,CONST.NUMBER_OF_SQUARES_IN_LENGTH));
		this.setBorder(BorderFactory.createLineBorder(Color.MAGENTA));
		for (int rowSquare = 0; rowSquare < CONST.NUMBER_OF_SQUARES_IN_LENGTH; rowSquare++) {
			for (int colSquare = 0; colSquare < CONST.NUMBER_OF_SQUARES_IN_LENGTH; colSquare++) {
				createSquare(rowSquare, colSquare);
			}
		}
	}

	private void createSquare(int rowSquare, int colSquare) {
		JPanel innerPanel = new JPanel(new GridLayout(CONST.NUMBER_OF_SQUARES_IN_LENGTH, CONST.NUMBER_OF_SQUARES_IN_LENGTH));
		innerPanel.setBorder(BorderFactory.createLineBorder(Color.RED));
		for (GridElementGUI gE : (GridElementGUI[]) grid.getSquare(rowSquare, colSquare))
			innerPanel.add(gE.getGui());
		this.add(innerPanel);
	}
}
