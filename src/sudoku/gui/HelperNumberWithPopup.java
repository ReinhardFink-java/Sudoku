package sudoku.gui;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JLabel;

import sudoku.CONST;
import sudoku.GridElement;

@SuppressWarnings("serial")
public class HelperNumberWithPopup extends JLabel {

	private GridElementGUI gridElementGUI;
	private PopupMenu popupMenu;
	private int number;
	private int standardFontSize;

	public HelperNumberWithPopup(int number, GridElementGUI gridElementGUI) {
		super();
		this.gridElementGUI = gridElementGUI;
		this.number = number;
		this.setText("" + (number + 1));
		this.setHorizontalAlignment(JLabel.CENTER);
		this.standardFontSize = this.getFont().getSize();
		this.reloadStates();
		this.popupMenu = new PopupMenu(number, gridElementGUI);
	}

	public void reloadStates() {
		this.setFont(new Font(this.getFont().getName(),	Font.PLAIN, this.standardFontSize));
		switch (gridElementGUI.getHelperStateForNumber(number)) {
		case POSSIBLE:
			this.setForeground(CONST.COLOR_POSSIBLE);
			break;
		case MARKED:
			this.setForeground(CONST.COLOR_MARKED);
			break;
		case BLOCKED:
			this.setForeground(CONST.COLOR_BLOCKED);
			break;
		case FIXED:
			//System.out.println("fixed");
			this.setForeground(CONST.COLOR_FIXED);
			this.setFont(new Font(this.getFont().getName(),	Font.ITALIC, this.standardFontSize * CONST.INCREASE_SIZE));
			break;
		case PROVIDED:
			//System.out.println("provided");
			this.setForeground(CONST.COLOR_PROVIDED);
			this.setFont(new Font(this.getFont().getName(),	Font.PLAIN, this.standardFontSize * CONST.INCREASE_SIZE));
			//this.setForeground(CONSTANTS.COLOR_PROVIDED);
			break;
		}
	}

	public void showPopupMenu(MouseEvent e) {
		popupMenu.show(e.getComponent(), e.getX(), e.getY());
	}
}
