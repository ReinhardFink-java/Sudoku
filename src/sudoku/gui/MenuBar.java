package sudoku.gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

import javax.swing.JFileChooser;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

import sudoku.CONST;
import sudoku.GridElement;
import sudoku.STATE;

@SuppressWarnings("serial")
public class MenuBar extends JMenuBar implements ActionListener {

	private SudokuPanel sudokuPanel;
	private JFileChooser fileChooser;

	public MenuBar(SudokuPanel sudokuPanel) {
		super();
		this.sudokuPanel = sudokuPanel;
		this.fileChooser = new JFileChooser();
		this.createMenuBar();
	}

	public void createMenuBar() {
		JMenu fileMenu = new JMenu(CONST.MENU_FILE);
		this.add(fileMenu);
		JMenuItem newMI = new JMenuItem(CONST.MENU_FILE_NEW);
		newMI.addActionListener(this);
		fileMenu.add(newMI);
		JMenuItem loadMI = new JMenuItem(CONST.MENU_FILE_LOAD);
		loadMI.addActionListener(this);
		fileMenu.add(loadMI);
		JMenuItem saveMI = new JMenuItem(CONST.MENU_FILE_SAVE);
		saveMI.addActionListener(this);
		fileMenu.add(saveMI);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getActionCommand().equals(CONST.MENU_FILE_NEW)) {
			sudokuPanel.getGridGUI().reset();
		}
		else if (e.getActionCommand().equals(CONST.MENU_FILE_LOAD)) {
			sudokuPanel.getGridGUI().reset();
			int returnVal = fileChooser.showOpenDialog(MenuBar.this);

			if (returnVal == JFileChooser.APPROVE_OPTION) {
				File file = fileChooser.getSelectedFile();
				// This is where a real application would open the file.
				try {
					Scanner scanner = new Scanner(file);
					GridElement[][] grid = sudokuPanel.getGridGUI().getGrid().getGrid();
					STATE[] states = new STATE[CONST.GRID_LENGTH];
					while (scanner.hasNext()) {
						//System.out.println(scanner.next());
						scanner.next();
						int row = scanner.nextInt() - 1;
						scanner.next();
						int col = scanner.nextInt() - 1;
						scanner.next();
						//System.out.println(row + " row col " + col);
						STATE state = STATE.createFromInt(scanner.nextInt());
						scanner.next();
						for (int i = 0; i < CONST.GRID_LENGTH; i++) {
							states[i] = STATE.createFromInt(scanner.nextInt());
							//System.out.println(scanner.nextInt());
						}
						grid[row][col].setHelperStates(states);
						grid[row][col].setState(state);
					}
					
					scanner.close();
				} catch (IOException io) {
					System.out.println("Exception ");
				}

			}
		}
		else if (e.getActionCommand().equals(CONST.MENU_FILE_SAVE)) {
			int returnVal = fileChooser.showSaveDialog(MenuBar.this);
			if (returnVal == JFileChooser.APPROVE_OPTION) {
				File file = fileChooser.getSelectedFile();
				try {
					FileWriter writer = new FileWriter(file);
					sudokuPanel.getGridGUI().getGrid();
					writer.write(sudokuPanel.getGridGUI().getGrid().toString());
					writer.close();
				} catch (IOException io) {
					System.out.println("Exception ");
				}
			}
		}
	}
}
