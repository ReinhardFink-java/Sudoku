package sudoku;

import javax.swing.*;

public class Message  {

    private static StringBuffer message;
    private static JTextArea loggingTextarea;

    static {
        message = new StringBuffer();
    }

    public static void connectJTextFieldToMessage(JTextArea loggingTextarea) {
        if (loggingTextarea != null)
            Message.loggingTextarea = loggingTextarea;
    }

    public static void append(String message) {
        Message.message.append(message);
    }

    public static void show() {
        if (loggingTextarea != null) {
            loggingTextarea.setText(Message.message.toString());
            loggingTextarea.repaint();
        }
    }

    public static void appendAndShow(String message) {
        Message.append(message);
        Message.show();
    }
}
