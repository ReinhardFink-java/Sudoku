package sudoku;

import java.util.Vector;

import sudoku.gui.GridElementGUI;

public class Grid {

	private GridElement[][] grid;

	public Grid(int rows, int cols) {
		super();
		this.grid = new GridElement[rows][cols];
	}

	public String toString() {
		StringBuffer temp = new StringBuffer(55 * CONST.GRID_LENGTH
				* CONST.GRID_LENGTH);
		for (int row = 0; row < CONST.GRID_LENGTH; row++)
			for (int col = 0; col < CONST.GRID_LENGTH; col++)
				temp.append(grid[row][col]).append("\n");
		return new String(temp);
	}

	public GridElement[][] getGrid() {
		return grid;
	}

	public void setElementAt(int row, int col, GridElementGUI element) {
		grid[row][col] = element;
	}

	public GridElement[] getSquare(int row, int col) {
		GridElement[] miniGrid = new GridElementGUI[CONST.GRID_LENGTH];
		for (int i = 0; i < CONST.GRID_LENGTH; i++)
			miniGrid[i] = grid[row * CONST.NUMBER_OF_SQUARES_IN_LENGTH + i / CONST.NUMBER_OF_SQUARES_IN_LENGTH][col
					* CONST.NUMBER_OF_SQUARES_IN_LENGTH + i % CONST.NUMBER_OF_SQUARES_IN_LENGTH];
		return miniGrid;
	}

	public GridElement[] getSquareContainingGridElemnetAt(int row, int col) {
		return getSquare(row / CONST.NUMBER_OF_SQUARES_IN_LENGTH, col / CONST.NUMBER_OF_SQUARES_IN_LENGTH);
	}

	public GridElement[] getRow(int row) {
		return grid[row];
	}

	public GridElement[] getCol(int col) {
		GridElement[] colum = new GridElement[CONST.GRID_LENGTH];
		for (int i = 0; i < CONST.GRID_LENGTH; i++)
			colum[i] = grid[i][col];
		return colum;
	}

	public boolean contains(GridElement testElement, GridElement[] connection) {
		for (GridElement gE : connection)
			if(gE.equals(testElement))
				return true;
		return false;
	}

	public GridElement[] getElementsHavingCollitionWithFixed(int row, int col) {
		Vector<GridElement> gEV = new Vector<GridElement>();
		int number = grid[row][col].getFixedNumber();
		for (GridElement gE : getSquareContainingGridElemnetAt(row, col))
			if (!grid[row][col].equals(gE) && gE.getFixedNumber() == number)
				gEV.add(gE);
		for (GridElement gE : getRow(row))
			if (!grid[row][col].equals(gE) && gE.getFixedNumber() == number)
				gEV.add(gE);
		for (GridElement gE : getCol(col))
			if (!grid[row][col].equals(gE) && gE.getFixedNumber() == number)
				gEV.add(gE);
		// Copy Elements to Array without loosing Type
		GridElement[] temp = new GridElement[gEV.size()];
		for (int i = 0; i < gEV.size(); i++)
			temp[i] = gEV.get(i);
		return temp;
	}

	public GridElement[] getEqualFixed(int row, int col) {
		Vector<GridElement> gEV = new Vector<>();
		int number = grid[row][col].getFixedNumber();
		for (int r = 0; r < CONST.GRID_LENGTH; r++)
			for (int c = 0; c < CONST.GRID_LENGTH; c++)
				if (grid[r][c].getState().isFixed()
						&& grid[r][c].getFixedNumber() == number)
					gEV.add(grid[r][c]);
		// Copy Elements to Array without loosing Type
		GridElement[] temp = new GridElement[gEV.size()];
		for (int i = 0; i < gEV.size(); i++)
			temp[i] = gEV.get(i);
		return temp;
	}

	public void reset() {
		for (int row = 0; row < CONST.GRID_LENGTH; row++)
			for (int col = 0; col < CONST.GRID_LENGTH; col++)
				grid[row][col].reset();
	}
}
