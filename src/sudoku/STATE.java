package sudoku;

public enum STATE {
    POSSIBLE,
    MARKED,
    BLOCKED,
    FIXED,
    PROVIDED;

    public static STATE createFromInt(int i) {
        switch (i) {
            case 0:
                return POSSIBLE;
            case 1:
                return MARKED;
            case 2:
                return BLOCKED;
            case 3:
                return FIXED;
            case 4:
                return PROVIDED;
            default:
                throw new IndexOutOfBoundsException();
        }

    }

    public boolean isPossible() { return this.ordinal() == POSSIBLE.ordinal(); }

    public boolean isMarked() { return this.ordinal() == MARKED.ordinal(); }
    public boolean isNotMarked() { return !isMarked(); }

    public boolean isPossibleOrMarked() { return this.ordinal() <= MARKED.ordinal(); }
    public boolean isNotPossibleOrMarked() { return isPossibleOrMarked(); }

    public boolean istBlocked() { return this.ordinal() == BLOCKED.ordinal(); }
    public boolean isNotBlocked() { return !istBlocked(); }

    public boolean isFixed() { return this.ordinal() >= FIXED.ordinal(); }
    public boolean isNotFixed() { return !isFixed(); }

    public boolean istBlockedOrFixed() { return this.ordinal() >= BLOCKED.ordinal(); }
    public boolean istNotBlockedOrFixed() { return !istBlockedOrFixed(); }


}
