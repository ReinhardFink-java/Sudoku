package sudoku;

/*
	GridElement represents 1 Number in the Sudoku Field:
	GridElement works intern with
 	index_inside 0 .. n-1
*/

public abstract class GridElement implements Cloneable {

	// coordinates in grid
	private int row;
	private int col;
	// STATE of this gridelement
	private STATE myState;
	// states of possible helper numbers
	private STATE[] helperStates;

	public GridElement(int row, int col) {
		super();
		this.col = col;
		this.row = row;
		this.myState = STATE.POSSIBLE;
		this.helperStates = new STATE[CONST.GRID_LENGTH];
		for (int i = 0; i <  CONST.GRID_LENGTH; i++) this.helperStates[i] = STATE.POSSIBLE;
		//System.out.println(this.states[0]);
	}

	public String toString() {
		StringBuffer temp = new StringBuffer(60);
		temp.append(String.format("row: %d col: %d state: %s states:",(row + 1),(col + 1), myState.ordinal()));
		for (STATE value : helperStates) temp.append(" " + value.ordinal());
		return new String(temp);
	}

	public Object clone() throws CloneNotSupportedException {
		GridElement gE = (GridElement)super.clone();
		gE.helperStates = this.helperStates.clone();
		return gE;
	}

	public int getRow() {
		return row;
	}

	public int getCol() {
		return col;
	}

	public STATE getState() {
		return myState;
	}

	public void setState(STATE state) {
		this.myState = state;
	}

	public void setHelperStates(STATE[] states) {
		this.helperStates = states.clone();
	}

	public STATE getHelperStateForNumber(int number) {
		return helperStates[number];
	}

	public void setHelperStateForNumber(int number, STATE newState) {
		if (this.myState.isNotFixed() && newState.isFixed()) blockAllHelperStates();
		if (this.myState.isFixed() && newState.isNotFixed()) resetAllHelperStates();
		this.myState = newState;
		helperStates[number] = newState;
	}

	public  boolean testForCalculatedFix() {
		if (this.myState.isNotFixed()) {
			int countBlocked = 0;
			for (int i = 0; i < CONST.GRID_LENGTH; i++)
				if (helperStates[i].istBlocked())
					countBlocked++;
			if (countBlocked == CONST.GRID_LENGTH - 1)
				return true;
		}
		return false;
	}

	public int getFixedNumber() {
		int i = 0;
		while (i < CONST.GRID_LENGTH && helperStates[i].isNotFixed())
			i++;
		// System.out.println("fix: " + (i+1));
		return i;
	}

	public void setCalculatedFix() {
		if (this.myState.isNotFixed()) {
			int i = 0;
			while (helperStates[i] == STATE.BLOCKED)
				i++;
			helperStates[i] = STATE.FIXED;
			myState = STATE.FIXED;
		}
	}

	public void resetAllHelperStates() {
		for (int i = 0; i < CONST.GRID_LENGTH; i++)
			helperStates[i] = STATE.POSSIBLE;
	}

	public void reset() {
		for (int i = 0; i < CONST.GRID_LENGTH; i++)
			helperStates[i] = STATE.POSSIBLE;
		myState = STATE.POSSIBLE;
	}

	public void blockAllHelperStates() {
		for (int i = 0; i < CONST.GRID_LENGTH; i++)
			helperStates[i] = STATE.BLOCKED;
	}
}
