package sudoku.solver;

import sudoku.*;

public class Solver {

	private Grid grid;
	private StringBuffer message;

	public Solver(Grid grid) {
		this.grid = grid;
		this.message = new StringBuffer();
	}

	public void resetAllHelperStates() {
		for (int row = 0; row < CONST.GRID_LENGTH; row++)
			for (int col = 0; col < CONST.GRID_LENGTH; col++) {
				if (grid.getGrid()[row][col].getState().isNotFixed())
					grid.getGrid()[row][col].resetAllHelperStates();
			}
	}

	/*
	 * Function blocks for every fixed gridelement
	 * all helper numbers in square, row and column
	 */
	public void blockHelperNumbersFromFixedElement() {
		//resetAllBlockedNumbers();
		Message.appendAndShow(CONST.MESSAGE_SHOW_BLOCKED);
		for (int row = 0; row < CONST.GRID_LENGTH; row++)
			for (int col = 0; col < CONST.GRID_LENGTH; col++) {
				GridElement fixGE = grid.getGrid()[row][col];
				if (fixGE.getState().isFixed()) {
					message.delete(0,message.length());
					message.append("BLOCK SQUARE:\t");
					blockNumberFromElementInConnection(fixGE,
							grid.getSquareContainingGridElemnetAt(fixGE.getRow(), fixGE.getCol()));
					message.delete(0,message.length());
					message.append("BLOCK ROW:\t\t");
					blockNumberFromElementInConnection(fixGE, grid.getRow(fixGE.getRow()));
					message.delete(0,message.length());
					message.append("BLOCK COL:\t\t");
					blockNumberFromElementInConnection(fixGE, grid.getCol(fixGE.getCol()));
				}
			}
	}

	/*
	 * Function checks, for every gridelement,
	 * if there is a unique helper number,
	 * which is NOT blocked when all others are blocked
	 * and sets unique helper number to FIXED.
	 */
	public void fixUniqueNotBlockedInElement() {
		Message.appendAndShow(CONST.MESSAGE_CALC_FIX_FROM_ELEMENT);
		for (int row = 0; row < CONST.GRID_LENGTH; row++)
			for (int col = 0; col < CONST.GRID_LENGTH; col++) {
				GridElement testedGE = grid.getGrid()[row][col];
				if (testedGE.testForCalculatedFix()) {
					testedGE.setCalculatedFix();
					Message.appendAndShow(String.format("FIXED:\t\t(%d/%d) <- %d\tweil alle anderen Hilfszahlen blockiert waren\n", row + 1, col + 1,testedGE.getFixedNumber()+1));
				}
			}
	}

	/*
	 * Function checks, for every square, row and column,
	 * if there is a unique helper number in one gridelement,
	 * which is NOT blocked in one unique gridelement
	 * while the same number is blocked in  all others gidelement in square, row or column
	 * and sets unique helper number to FIXED.
	 */
	public void fixElementsFromBlockedInRowColSquare() {
		//setBlockedStatesFromElements();
		Message.appendAndShow(CONST.MESSAGE_CALC_FIX_FROM_CONNECTION);
		for (int row = 0; row < CONST.GRID_LENGTH; row++)
			for (int col = 0; col < CONST.GRID_LENGTH; col++) {
				GridElement testedGE = grid.getGrid()[row][col];
				message.delete(0,message.length());
				message.append("FIX SQUARE:\t\t");
				fixNumberFromConnection(testedGE, grid.getSquareContainingGridElemnetAt(row, col));
				message.delete(0,message.length());
				message.append("FIX ROW:\t\t");
				fixNumberFromConnection(testedGE, grid.getRow(row));
				message.delete(0,message.length());
				message.append("FIX COL:\t\t");
				fixNumberFromConnection(testedGE, grid.getCol(col));
			}
	}

	/*
	 * Function blocks all helper Numbers,
	 * equal to a fixed number,
	 * which are in the same square, row or column
	 */
	private void blockNumberFromElementInConnection(GridElement fixGridElement,
			GridElement[] connection) {
		message.append(String.format("(%d/%d) = %d => ",fixGridElement.getRow() + 1,fixGridElement.getCol() + 1,fixGridElement.getFixedNumber() + 1));
		for (GridElement gE : connection) {
			if (!gE.equals(fixGridElement) // do not work on yourself
				//&& gE.getState().isNotFixed()
				&& (gE.getHelperStateForNumber(fixGridElement.getFixedNumber()).isPossible())
					|| gE.getHelperStateForNumber(fixGridElement.getFixedNumber()).isMarked()) {
				gE.setHelperStateForNumber(fixGridElement.getFixedNumber(),
						STATE.BLOCKED);
				message.append(String.format("(%d/%d) ", gE.getRow() + 1, gE.getCol() + 1));
			}
		}
		// check for more tha one "("
		if (message.indexOf("(") < message.lastIndexOf("(")) {
			message.append("\n");
			Message.appendAndShow(message.toString());
		}
	}

	/*
	 * Function tests, for testedGE
	 * if numberToTest is blocked in all gridelements
	 * in row, column or square
	 */
	private void fixNumberFromConnection(GridElement testedGE,
			GridElement[] connection) {
		for (int numberToTest = 0; numberToTest < CONST.GRID_LENGTH; numberToTest++) {
			// Test, if GridElement can be set
			if (testedGE.getHelperStateForNumber(numberToTest).isPossible()
				||
				testedGE.getHelperStateForNumber(numberToTest).isMarked()) {
				int count = 0;
				for (GridElement gE : connection) {
					// Test, if number already fix in Grid and ...
					if (gE.getHelperStateForNumber(numberToTest).isFixed())
						return;
					if (gE.getState().isFixed()									// fixed element
						||
						gE.getHelperStateForNumber(numberToTest).istBlocked())	// numberToTest is blocked
						count++;
				}
				if (count == CONST.GRID_LENGTH - 1) {
					testedGE.setHelperStateForNumber(numberToTest, STATE.FIXED);
					message.append(String.format("(%d/%d) <- %d\tweil diese Zahl in allen anderen Elementen blockiert war",testedGE.getRow() + 1,testedGE.getCol() + 1,testedGE.getFixedNumber() + 1));
				}
			}
		}
		if (message.indexOf("(") >= 0) {
			message.append("\n");
			Message.appendAndShow(message.toString());
		}
	}

	/*
	 * Function blocks grid elements in ines (= row or column)
	 * with  doubles or tribbles from one line in a square
	 */
	public void setBlockedElementsInLinesFromSqaures() {
		//setBlockedStatesFromElements();
		// System.out.println("-------------------");
		Message.appendAndShow(CONST.MESSAGE_BUTTON_CALC_FIX_FROM_SQUARE);
		boolean hasGridChanged = true;
		while (hasGridChanged) {
			hasGridChanged = false;
			for (int row = 0; row < CONST.NUMBER_OF_SQUARES_IN_LENGTH; row++)
				for (int col = 0; col < CONST.NUMBER_OF_SQUARES_IN_LENGTH; col++) {
					Square square = new Square(grid, grid.getSquare(row, col));
					hasGridChanged = square.blockNumbersInLines() || hasGridChanged;
				}
		}
	}

	public void setBlockedElementsFromDoublesDoublesInConnections() {
		// System.out.println("-------------------");
		for (int row = 0; row < CONST.GRID_LENGTH; row++) {
			Connection connection = new Connection(grid.getRow(row));
			connection.blockNumbersFromDoubleDouble();
		}
		for (int col = 0; col < CONST.GRID_LENGTH; col++) {
			Connection connection = new Connection(grid.getCol(col));
			connection.blockNumbersFromDoubleDouble();
		}
		for (int row = 0; row < CONST.NUMBER_OF_SQUARES_IN_LENGTH; row++)
			for (int col = 0; col < CONST.NUMBER_OF_SQUARES_IN_LENGTH; col++) {
				Connection connection = new Connection(grid.getSquare(row, col));
				connection.blockNumbersFromDoubleDouble();
			}
	}
}
