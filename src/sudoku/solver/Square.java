package sudoku.solver;


import sudoku.*;

/*
 * represents Square in grid
 */
public class Square {

	private Grid grid;
	private GridElement[] square;
	private GridElement[] oldSquare;
	StringBuffer message;

	public Square(Grid grid, GridElement[] square) {
		this.grid = grid;
		this.square = square;
		try {
			this.oldSquare = new GridElement[CONST.SQUARE_ELEMENTS];
			for (int i = 0; i < CONST.SQUARE_ELEMENTS; i++)
				oldSquare[i] = (GridElement)square[i].clone();
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}
		this.message = new StringBuffer();
	}

	public boolean blockNumbersInLines() {
		boolean isGridChanged = false;
		for (int numberToTest = 0; numberToTest < CONST.NUMBERS; numberToTest++)
			if (this.hasMoreThanTwoNotBlocked(numberToTest)) {
				// TODO
				// Achtung auch Doppelte. die nicht blocken werden markiert
				// BUG oder FEATURE
				//markDoubleOrTripple(numberToTest);
				isGridChanged = blockIfPossibleFromDoubleInRowCol(numberToTest)
						|| isGridChanged;
			}
		return isGridChanged;
	}	

	/*
	 * check for given number, if there are more than one
	 * not blocked or not fixed gridelements
	 */
	private boolean hasMoreThanTwoNotBlocked(int numberToTest) {
		int numberOfNotBlockedInSquare = 0;
		for (GridElement gE : square)
			if (gE.getHelperStateForNumber(numberToTest).istNotBlockedOrFixed())
				numberOfNotBlockedInSquare++;
		// more than 2, but maximal SQUARE_LENGTH
		return (numberOfNotBlockedInSquare >= 2 && numberOfNotBlockedInSquare <= CONST.SQUARE_LENGTH);
	}

	private boolean _blockIfPossibleFromDoubleInRowCol(int numberToTest) {
		for (GridElement gFirst : this.square)
			for (GridElement gSecond : this.square)
				if (!gFirst.equals(gSecond) &&
						gFirst.getHelperStateForNumber(numberToTest).istNotBlockedOrFixed() &&
						gSecond.getHelperStateForNumber(numberToTest).istNotBlockedOrFixed()) {
					if (gFirst.getRow() == gSecond.getRow())
						return (blockNumberInLine(numberToTest, grid.getRow(gFirst.getRow())) > 0);
					if (gFirst.getCol() == gSecond.getCol())
						return (blockNumberInLine(numberToTest, grid.getRow(gFirst.getCol())) > 0);
				}
		return false;
	}

	private boolean blockIfPossibleFromDoubleInRowCol(int numberToTest) {
		int row;
		int col;
		// find 1. marked
		int i = 0;
		while (i < CONST.SQUARE_ELEMENTS
				&& square[i].getHelperStateForNumber(numberToTest).istBlockedOrFixed())
			i++;
		row = square[i].getRow();
		col = square[i].getCol();
		i++;
		// find next marked in Line
		while (i < CONST.SQUARE_ELEMENTS) {
			while (i < CONST.SQUARE_ELEMENTS
					&& square[i].getHelperStateForNumber(numberToTest).istBlockedOrFixed())
				i++;
			if (i < CONST.SQUARE_ELEMENTS && row != square[i].getRow())
				row = CONST.NO_ROW;
			if (i < CONST.SQUARE_ELEMENTS && col != square[i].getCol())
				col = CONST.NO_COL;
			i++;
		}
		// System.out.println("row: " + row);
		// important rowcol[0] != -1 and rowcol[1] != -1 can not appear at same time
		message.delete(0, message.length());
		boolean hasRowChanged = false;
		if (row != CONST.NO_ROW) {
			message.append(String.format("BLOCK ROW:\t\t"));
			markDoubleOrTripple(numberToTest);
			hasRowChanged = blockNumberInLine(numberToTest, grid.getRow(row)) > 0;
		}
		boolean hasColChanged = false;
		if (col != CONST.NO_COL) {
			message.append(String.format("BLOCK COL:\t\t"));
			markDoubleOrTripple(numberToTest);
			hasColChanged = blockNumberInLine(numberToTest, grid.getCol(col)) > 0;
		}
		if (hasRowChanged || hasColChanged)
			return true;
		else // reset new MARKED, but not blocking grid elements back to POSSIBLE
			for (int j = 0; j < CONST.SQUARE_ELEMENTS; j++) {
				if (oldSquare[j].getHelperStateForNumber(numberToTest).isNotMarked() &&
						square[j].getHelperStateForNumber(numberToTest).isMarked())
					square[j].setHelperStateForNumber(numberToTest, STATE.POSSIBLE);
			}
		return false;
	}

	private void markDoubleOrTripple(int numberToTest) {
		for (GridElement gE : square) {
			if (gE.getHelperStateForNumber(numberToTest).isPossible()) {
				gE.setHelperStateForNumber(numberToTest, STATE.MARKED);
				message.append(String.format("(%d/%d) ",gE.getRow() + 1, gE.getCol() + 1));
			}
		}
		message.append("= " + (numberToTest + 1) + " -> ");
	}

	private boolean contains(GridElement gridElement) {
		for (GridElement gE : square)
			if (gE.equals(gridElement))
				return true;
		return false;
	}

	/*
	 * block number number in all gridelements gE
	 * in given line (=row or column)
	 */
	private int blockNumberInLine(int number, GridElement[] line) {
		int countNewBlocked = 0;
		for (GridElement gE : line) {
			if (!this.contains(gE)
					&& gE.getHelperStateForNumber(number).istNotBlockedOrFixed()) {
				gE.setHelperStateForNumber(number, STATE.BLOCKED);
				message.append(String.format("(%d/%d) ",gE.getRow() + 1, gE.getCol() + 1));
				countNewBlocked++;
			}
		}
		// check for more tha one "("
		if (countNewBlocked > 0)
			Message.appendAndShow(message.toString()+"\n");
		return countNewBlocked;
	}
}
