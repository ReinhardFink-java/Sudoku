package sudoku.solver;


import sudoku.CONST;
import sudoku.GridElement;
import sudoku.STATE;

public class Connection {

	private GridElement[] connection;

	public Connection(GridElement[] connection) {
		this.connection = connection;
	}
	
	public void blockNumbersFromDoubleDouble() {
		int[] positionsOfNumber = new int[CONST.GRID_LENGTH];
		for (int numberToTest = 0; numberToTest < CONST.GRID_LENGTH; numberToTest++)
			if (hasDouble(numberToTest)) {
				collectPositionsOfNumber(numberToTest, positionsOfNumber);
				blockIfPossibleFromDoubleDouble(positionsOfNumber);
			}
	}
	
	private boolean hasDouble(int numberToTest) {
		int countFree = 0;
		for (int i = 0; i < CONST.GRID_LENGTH; i++) {
			if (connection[i].getHelperStateForNumber(numberToTest).isNotBlocked()) {
				countFree++;
			}
		}
		return (countFree == 2);
	}

	private void collectPositionsOfNumber(int numberToTest, int[] positionsOfNumber) {
		int factor = 1;
		for (int i = 0; i < CONST.GRID_LENGTH; i++)
			if (connection[i].getHelperStateForNumber(numberToTest).isNotBlocked()) {
				positionsOfNumber[numberToTest] += i * factor;
				factor *= CONST.GRID_LENGTH;
			}
	}

	private void blockIfPossibleFromDoubleDouble(int[] positionsOfNumber) {
		for (int lower = 0; lower < CONST.GRID_LENGTH - 1; lower++)
			for (int upper = lower + 1; upper < CONST.GRID_LENGTH; upper++)
				if (positionsOfNumber[lower] > 0 
						&& positionsOfNumber[upper] > 0 
						&& positionsOfNumber[lower] == positionsOfNumber[upper]) {
					int firstPosition = positionsOfNumber[lower] % CONST.GRID_LENGTH;
					int secondPosition = positionsOfNumber[lower] / CONST.GRID_LENGTH;
					connection[firstPosition].blockAllHelperStates();
					connection[firstPosition].setHelperStateForNumber(lower, STATE.MARKED);
					connection[firstPosition].setHelperStateForNumber(upper, STATE.MARKED);
					connection[secondPosition].blockAllHelperStates();
					connection[secondPosition].setHelperStateForNumber(lower, STATE.MARKED);
					connection[secondPosition].setHelperStateForNumber(upper, STATE.MARKED);
				}
	}

}
