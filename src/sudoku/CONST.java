package sudoku;

import java.awt.Color;


public class CONST {

	public final static int SQUARE_LENGTH = 3;
	public final static int SQUARE_ELEMENTS = SQUARE_LENGTH * SQUARE_LENGTH;

	public final static int NUMBER_OF_SQUARES_IN_LENGTH = 3;
	public final static int GRID_LENGTH = NUMBER_OF_SQUARES_IN_LENGTH * NUMBER_OF_SQUARES_IN_LENGTH;

	public final static int NUMBERS = SQUARE_ELEMENTS;
	
	public static final int INCREASE_SIZE = 2;
	
	public final static int NO_ROW = -1;
	public final static int NO_COL = -1;

	public static final String LABEL_POSSIBLE = "nicht markiert";
	public static final String LABEL_MARKED = "markiert";
	public static final String LABEL_BLOCKED = "blockiert";
	public static final String LABEL_FIXED = "fixed";
	public static final String LABEL_PROVIDED = "vorgegeben";

	public static final String LABEL_BUTTON_RESET_BLOCKED = "<html>Markierungen<br>zurücksetzten</html>";

	public static final String MESSAGE_HEADER = "#############################################################\n";

	public static final String LABEL_BUTTON_SHOW_BLOCKED = "<html>Blockiere mit<br>fixen Zahlen</html>";
	public static final String MESSAGE_SHOW_BLOCKED = MESSAGE_HEADER + "Blockiere mit fixen Zahlen\n";

	public static final String LABEL_BUTTON_CALC_FIX_FROM_ELEMENT = "<html>Berechne fixe Zahl<br>in einem Element</html>";
	public static final String MESSAGE_CALC_FIX_FROM_ELEMENT = MESSAGE_HEADER + "Berechne fixe Zahl in einem Element\n";

	public static final String LABEL_BUTTON_CALC_FIX_FROM_CONNECTION = "<html>Berechne fixe Zahl in<br>Quadrat/Zeile/Spalte</html>";
	public static final String MESSAGE_CALC_FIX_FROM_CONNECTION = MESSAGE_HEADER + "Berechne fixe Zahl in Quadrat/Zeile/Spalte\n";

	public static final String LABEL_BUTTON_CALC_FIX_FROM_SQUARE = "<html>Blockiere in Zeilen/Spalten<br>mit Zwillingen/Drilligen<br>aus Quadraten.\n";
	public static final String MESSAGE_BUTTON_CALC_FIX_FROM_SQUARE = MESSAGE_HEADER + "Blockiere in Zeilen/Spalten mit Zwillingen/Drilligen aus Quadraten.\n";

	public static final Color COLOR_BLOCKED = Color.RED;
	public static final Color COLOR_POSSIBLE = Color.WHITE;
	public static final Color COLOR_MARKED = Color.BLUE;
	public static final Color COLOR_FIXED = Color.BLUE;
	public static final Color COLOR_PROVIDED = Color.BLACK;
	
	public static final Color COLOR_EQUALS = Color.GREEN;

	
	public static final Color COLOR_BACKGROUND = new Color(238,238,238);
	public static final Color COLOR_HIGHLIGHT = new Color(255,240,240);
	
	public static final Color COLOR_GRID_ELEMENT_BORDER = Color.YELLOW;
	
	public static final String MENU_FILE = "File";
	public static final String MENU_FILE_NEW = "New";
	public static final String MENU_FILE_LOAD = "Load";
	public static final String MENU_FILE_SAVE = "Save";

}
